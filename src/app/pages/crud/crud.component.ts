import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormBuilder, FormArray, FormGroup, FormControl } from '@angular/forms';
import { HomeService } from '../home/home.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-crud',
  templateUrl: './crud.component.html',
  styleUrls: ['./crud.component.css']
})
export class CrudComponent implements OnInit {

  cadastroForm: FormGroup;
  id: any;
  acao: any;
  myDate: Date;

  constructor(private formBuilder: FormBuilder,
    private homeService: HomeService,
    private activatedRoute: ActivatedRoute,
    private cdRef: ChangeDetectorRef,
    private router: Router) {

    this.id = activatedRoute.snapshot.paramMap.get("id");
    this.acao = activatedRoute.snapshot.paramMap.get("acao");
    this.myDate = new Date();
    console.log(this.id);
    console.log(this.acao);
  }

  ngOnInit(): void {
      this.cadastroForm = this.formBuilder.group({
        nome: '',
        cpf: '',
        dataNascimento: '',
        contatos: this.formBuilder.array([])
      });

      if (this.id != null && this.id != undefined) {
        this.homeService.getPessoa(this.id)
        .then(pessoa => {
            this.cadastroForm.patchValue({
              nome: pessoa.nome,
              cpf: pessoa.cpf,
            });
            this.myDate = pessoa.dataNascimento;  
            pessoa.contatos.forEach((value) => {
            this.addContato(value.nome, value.telefone, value.email);
          });
        }).catch(error => window.alert(error));
    } 
  }

  get contatosArrayControl() {
    return (this.cadastroForm.get('contatos') as FormArray).controls;
  }

  addContato(nomeContato: String, telefoneContato: String, emailContato: String ) {
    const contatosArray = (this.cadastroForm.get('contatos') as FormArray);

    contatosArray.push(this.formBuilder.group({
      nome: nomeContato,
      telefone: telefoneContato,
      email: emailContato
    }));
  }

  removeContato(index: any) {
    const contatosArray = (this.cadastroForm.get('contatos') as FormArray);
    contatosArray.removeAt(index);
  }

  onSubmit(): void {
    console.warn(this.cadastroForm.value);
    if (this.id === null || this.id === undefined) {
      this.homeService.salvarPessoa(this.cadastroForm.value)
      .then(response => {
        console.log(response);
        window.alert('Cadastro salvo!');
        this.router.navigate(['/home']);
      }).catch(error => {
        console.log(error);
        window.alert(error.error);
      });
    } else {
      this.homeService.atualizarPessoa(this.id, this.cadastroForm.value)
      .then(response => {
        console.log(response);
        window.alert('Cadastro atualizado!');
        this.router.navigate(['/home']);
      }).catch(error => {
        console.log(error);
        window.alert(error.error);
      });
    }
  }

  voltar() {
    this.router.navigate(['/home']);
  }
  

}
