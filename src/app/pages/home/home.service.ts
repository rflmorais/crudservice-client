import { Injectable } from '@angular/core';
import { Pessoa } from './Pessoa';
import { HttpClient } from '@angular/common/http';
import { url } from "./../../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  pessoas: Pessoa[] = [{
    id: 1,
    nome: 'Rafael Morais',
    cpf: '43546434803',
    dataNascimento: new Date,
    contatos: [{
      id: 1,
      nome: 'Rafael Morais',
      email: 'rafael@morais.com',
      telefone: '19992701691'
    }]
  }];

  constructor(private http: HttpClient) {}

  salvarPessoa(cadastro: Pessoa) {
    return this.http.post<Pessoa>(url.URL_API + "api/public/v1/pessoa/cadastrar", cadastro, {headers: {'Content-Type': 'application/json'}}).toPromise();
  }

  removerPessoa(id: number) {
    return this.http.delete<Pessoa>(url.URL_API + "api/public/v1/pessoa/remover/" + id, {}).toPromise()
  }

  getPessoa(id: number) {
    return this.http.get<Pessoa>(url.URL_API + "api/public/v1/pessoa/obter/" + id, {}).toPromise();
  }

  atualizarPessoa(id: number, pessoa: Pessoa) {
    var p = this.pessoas.find((p: Pessoa) => p.id === id);
    p = pessoa;
    return this.http.put<Pessoa>(url.URL_API + "api/public/v1/pessoa/alterar/" + id, pessoa, {headers: {'Content-Type': 'application/json'}}).toPromise();
  }

  filtrar(filtro: any) {
    return this.http.post<Pessoa>(url.URL_API + "api/public/v1/pessoa/filtrar?page=0&size=10", filtro, {headers: {'Content-Type': 'application/json'}}).toPromise();
  }
}
