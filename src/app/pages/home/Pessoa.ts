export interface Pessoa {
    id : number,	
    nome:string,	
    cpf:string,	
    dataNascimento:Date,
    contatos:Array<Contato>
  }

export interface Contato {
    id : number,	
    nome:string,	
    telefone:string,	
    email:string
  }