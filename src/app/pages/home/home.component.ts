import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HomeService } from './home.service';
import { Pessoa } from './Pessoa';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  private pessoas: any;

  constructor(private router: Router, private homeService: HomeService, private datePipe: DatePipe) {
  }

  ngOnInit(): void {
    this.homeService.filtrar({})
    .then(pessoas => {
      this.pessoas=pessoas;
    }).catch(error => console.log(error));
  }

  get obterPessoas() {
    return this.pessoas;
  }

  filtrar(codigo: string, nome: string, cpf: string, dataNascimento: string) {
    const filtro = {
      idPessoa: codigo,
      nome: nome,
      cpf: cpf.replace(".", "").replace(".", "").replace("-", ""),
      dataNascimento: this.datePipe.transform(dataNascimento, 'yyyy-MM-dd')
    }
    console.log(filtro);
    this.homeService.filtrar(filtro)
    .then(pessoas => {
      this.pessoas=pessoas;
    }).catch(error => console.log(error));
  }

  abrir(pessoa: Pessoa) {
    console.log(pessoa);
    this.router.navigate(['/cadastro', pessoa.id, 'abrir']);
  }

  editar(pessoa: Pessoa) {
    console.log(pessoa);
    this.router.navigate(['/cadastro', pessoa.id, 'alterar']);
  }

  excluir(pessoa: Pessoa) {
    this.homeService.removerPessoa(pessoa.id)
    .then(response => {
      window.alert("Cadastro Excluído!")
      location.reload();
    }).catch(error => console.log(error));
  }

  cadastrar() {
    this.router.navigate(['/cadastro']);
  }

}
