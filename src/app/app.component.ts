import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'crudservice-client';

  constructor(private router: Router){
    this.goToHome();
  }

  goToHome() {
    this.router.navigate(['/home']);
  }

}
