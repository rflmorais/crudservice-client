# CrudService-Client

## Pré-requisitos

Esta é uma aplicação de frontend que deve ser executada em um servidor web.
Para seu pleno funcionamento necessita que o serviço [CrudService](https://bitbucket.org/rflmorais/crudservice/src/master/) esteja rodando no backend.

## Desenvolvimento

Dependências e versões para desenvolvimento e execução local.

| Dependência | versão  |
| ---         |  ----   |
| npm         | 6.14.8  |
| node        | 10.23.0 |
| agular CLI  | 11.0.4  |

 
- Antes de iniciar o desenvolvimento executar o comando **_npm install_** para instalar as bibliotecas.

## Executando a aplicação

### Localmente
Para executar localmente utilize o comando **_ng serve_**.

Neste caso utilizar a URL abaixo para acessar o client:
```
http://localhost:4200/home
```

### Via Contêiner
Acesse a raiz da pasta do projeto e execute os seguintes comandos:

**Docker Compose**
- Realize o build: **_docker-compose up --build_**

Neste caso utilizar a URL abaixo para acessar o client:
```
http://localhost/home
```

## Parâmetros de Configuração
Os parâmetros que são mutáveis no sistema podem ser encontrados no arquivo de configuração **environment.ts** (desenvolvimento) e **environment.prod.ts** (produção).

As URLs dos serviços são configuradas na constante **url**, onde:
* **URL_API**: variável que indica a localização do projeto [CrudService](https://bitbucket.org/rflmorais/crudservice/src/master/);

```
export const url = {
  URL_API: "http://localhost:8110/"
}
```

### Atenção

Quando realizadas alterações nos arquivos **environment.ts** e **environment.prod.ts**, é necessário compilar a aplicação para executar as alterações.